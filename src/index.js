import Resolver from '@forge/resolver';
import {fetch, storage} from '@forge/api';

import ForgeUI, {GlobalSettings, MacroConfig, render, TextField, Form, useState} from '@forge/ui';

const resolver = new Resolver();

resolver.define("fetchDataFromGitlab", async ({payload, context}) => {
	const gitlabUrl = await storage.get('gitlabUrl');
	const gitlabAccessToken = await storage.get('gitlabAccessToken');

	const options = {}
	options.headers = {
		"PRIVATE-TOKEN": gitlabAccessToken
	}

	const result = await fetch(gitlabUrl + "/api/v4/projects/" + context.extension.config.gitlabProjectId + "/releases?include_html_description=true", options)

	const status = await result.status
	if (result.ok) {
		return result.json()
	} else {
		return "An error occurred"
	}
	// return "Ok";
});

export const handler = resolver.getDefinitions();
const Config = () => {
	return (
		<MacroConfig>
			<TextField name="gitlabProjectId" label="Gitlab Project ID"/>
		</MacroConfig>
	);
};

export const config = render(<Config/>);

const onSubmit = async (formData) => {

	const options = {}
	options.headers = {
		"PRIVATE-TOKEN": formData['gitlabAccessToken']
	}
	console.log(formData)
	const result = await fetch(formData['gitlabUrl'] + "/api/v4/user/status", options)
	if (result.ok){
		console.log("Success")
		console.log(result)
		await storage.set("gitlabUrl", formData['gitlabUrl'])
		await storage.set("gitlabAccessToken", formData['gitlabAccessToken'])
	} else {
		console.error(result)
		throw Error(result.status + " - " + result.statusText)
	}
};

export const globalConfig = render(
		<GlobalSettings>
			<Form onSubmit={onSubmit}>
				<TextField name={"gitlabUrl"} label={"Gitlab URL"} defaultValue={""}/>
				<TextField name={"gitlabAccessToken"} label={"Gitlab Access Token"} defaultValue={""}/>
			</Form>
		</GlobalSettings>
);

