import React, { useEffect, useState } from 'react';
import { invoke } from '@forge/bridge';

function App() {
  const [data, setData] = useState(null);
  useEffect(() => {
    invoke('fetchDataFromGitlab')
    .then((response) => {
      setData(response)
    })
  }, []);

  return (
    <div>
      {data ?
            data.map(gitlabReleaseNote => (
              <div key={gitlabReleaseNote.name}>
                <h3>{gitlabReleaseNote.name}</h3>
                <p><strong>Released On:</strong>{gitlabReleaseNote.released_at.substring(0, 10)}</p>
                <p><strong>Released By:</strong>{gitlabReleaseNote.author.name}</p>
                <br/>
                <p dangerouslySetInnerHTML={{__html: gitlabReleaseNote.description_html}} />
                <br/>
             </div>
            ))
        : 'Loading...'}
    </div>
  );
}

export default App;